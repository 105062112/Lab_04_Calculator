var expression = '';
var prev;
function enter(key) {
    var screen = document.getElementById('screen');
    if (key.value === 'C') {
        screen.value = 0;
        expression = prev = '';
    } else if (key.value === '=') {
        prev = '=';
        expression = expression.replace(/×/g, '*').replace(/÷/g, '/');
        try {
            screen.value = eval(expression);
            expression = String(screen.value);
        } catch (err) {
            screen.value = 'ERROR';
            expression = '';
        }
    } else if (key.value === '+' || key.value === '-' || key.value === '×' || key.value === '÷') {
        expression += String(key.value);
        screen.value = expression;
        prev = '';
    } else {
        if (prev === '=') {
            expression = prev = '';
        }
        expression += String(key.value);
        screen.value = expression;
    }
}